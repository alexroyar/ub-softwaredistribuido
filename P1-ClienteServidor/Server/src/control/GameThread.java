package control;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import model.GameConstants;
import model.Game;

/**
 *
 * @author Juan Manuel Morales Garzón
 */
public class GameThread extends Thread {
    private ServerSocket server;
    private Communication com;
    private Game game;

    private int id;
    private String in;
    private boolean ended = false;
    private boolean finish = false;
    private boolean signal = false;

    /**
     * Constructor of class GameThread. It initialises the lists and variables
     * that will be used on the game.
     * @param id the id of the game.
     * @param socket the socket that will be used.
     */
    public GameThread(int id, ServerSocket socket) {
        this.id = id;
        this.server = socket;
    }
    @Override
    public void run() {
        wait_for_signal();
    }
    
    /**
     * this is used to know the ID of the current game.
     * @return returns the Id of the current game.
     */
    @Override
    public long getId(){
        return this.id;
    }
    /**
     * Method used to wait until a client connects to our server.
     * When the client connects to the thread, then the Core will create another
     * Thread to wait for more clients.
     * @param socket 
     */
    private void wait_for_signal() {
        try {
            gameCore(this.server.accept());
        } catch (IOException ex) {
            System.err.println("La connexió no ha pogut ser establerta.");
            
        }
        this.ended = true;
            /*try {
                end();
            } catch (IOException ex1) {
                System.err.println("No s'ha pogut tancar la connexió");
            }*/
    }
    
    /**
     * Init the socket, and starts the first movements of the play.
     * @param socket
     * @throws IOException 
     */
    private void gameCore(Socket socket) throws IOException {
        System.out.println("Connexió establerta");
        try {
            init(socket);
        } catch (IOException ex) {
            System.err.println("Connexio perduda");
        }
        this.com.init_log();
        System.out.println("read cards");
        this.game.readCards("/home/alex/Proyectos/ub-sd-clienteservidor/Server/src/Resources/deck.txt");
        System.out.println("BEGIN PROTOCOL");
        in = com.receiveID();
        System.out.println(in);
        this.com.write_log("C: "+in);
        if (in.equalsIgnoreCase(GameConstants.START)){
            com.sendBet();
            while(!finish){
                String inn = com.receivePlayerMove();
                System.out.println(inn);
                this.com.write_log("C: "+inn);
                switch (inn.toLowerCase()) {
                    case GameConstants.DRAW:
                        com.sendCard();
                        break;
                    case GameConstants.ANTE:
                        int v = com.receiveAnte();
                        this.game.increaseBet(v);
                        System.out.println(" "+v);
                        inn = com.receiveID(); // we wait for a DRAW SIGNAL
                        if (inn.equalsIgnoreCase(GameConstants.DRAW)){
                            System.out.print(inn);
                            com.sendCard();
                        } else {
                            // throws an error.
                            System.err.println("ID draw expected");
                        }   break;
                    case GameConstants.PASS:
                        //server plays and end game.
                        System.out.println("");
                        com.sendBankScore(); // also sends gains.
                        System.out.println("");
                        finish = true;
                        break;
                }
            }
        }
        this.com.close_log();
    }

    /**
     * Initializes the GameThread to start a new game.
     * @param socket
     * @throws IOException 
     */
    private void init(Socket socket) throws IOException {       
        this.signal = true;
        this.game = new Game(this.id);
        this.com = new Communication(socket, this.game);
        
    }
    
    /* ===================================================
     * PLAYING METHODS
     * ===================================================
     */
    /**
     * returns the signal that marks this game as an active and not finished
     * @return signal
     */
    public boolean signal(){
        return signal;
    }
 
    /* ===================================================
     * END PLAYING METHODS
     * ===================================================
     */
    
    /**
     * return a boolean variable that indicate that the game has ended.
     * @return 
     */
    public boolean isEnded(){
        return this.ended;
    }
    /**
     * Ends the connection with the client.
     * @throws IOException 
     */
    public void end() throws IOException {
    }
}
