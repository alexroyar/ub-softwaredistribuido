package control;

import java.io.IOException;
import java.net.Socket;
import model.Card;
import model.Game;
import model.GameConstants;

/**
 *
 * @author admin
 */
public class Communication {
    private ComUtils com;
    private Game game;
    
    public Communication(Socket sckt, Game game) throws IOException{
        this.com = new ComUtils(sckt);
        this.game = game;
    }
    
    /* #########################################################################
     * SENDING METHODS
     * #########################################################################
     */
    
    public void sendBet() throws IOException{
        this.com.writeID(GameConstants.STARTING_BET, true);
        this.com.write_int32(this.game.getBet());
        System.out.println(GameConstants.STARTING_BET+ " " + this.game.getBet());
        this.com.write_log(this.game.getID(), "S: "+GameConstants.STARTING_BET+" "+this.game.getBet());
    }
    
    public void sendCard() throws IOException{
        this.com.writeID(GameConstants.CARD, true);
        this.com.writeCard(this.game.addPlayerCard());
        System.out.println(GameConstants.CARD + " "+ this.game.getPlayerCards().getLast().toString());
        this.com.write_log(this.game.getID(), "S: "+GameConstants.CARD + " "+ this.game.getPlayerCards().getLast().toString());
        if (this.game.getPlayerScore() > GameConstants.MAX_VALUE)
            sendBusting();
    }
    
    public void sendBusting() throws IOException{
        this.com.writeID(GameConstants.BUSTING, false);
        System.out.println(GameConstants.BUSTING);
        this.com.write_log(this.game.getID(), "S: "+GameConstants.BUSTING);
        sendBankScore();
    }
    
    public void sendBankScore() throws IOException{
        this.com.writeID(GameConstants.BANK_SCORE, true);
        this.game.serverPlay();
        this.com.write_int32(this.game.getServerCards().size());
        System.out.print(GameConstants.BANK_SCORE+" "+this.game.getServerCards().size());
        String str = new String();
        for (Card card:this.game.getServerCards()){
            this.com.writeCard(card);
            str += card.toString();
        }
        str += " ";
        this.com.write_byte((byte)' ');
        if (this.game.getServerScore() <= 10.0){
            this.com.write_byte((byte)'0');
            str += "0";
        }
        str += String.valueOf(this.game.getServerScore());
        System.out.println("\nValueOF"+String.valueOf(this.game.getServerScore()));
        this.com.write_string(String.valueOf(this.game.getServerScore()));
        
        System.out.println(" "+ this.game.getServerScore());
        this.com.write_log(this.game.getID(), "S: "+GameConstants.BANK_SCORE+" "+
                this.game.getServerCards().size()+str);
        if (this.game.getPlayerScore() > GameConstants.MAX_VALUE)
            sendGain(false);
        else
            if (this.game.getPlayerScore() > GameConstants.MAX_VALUE && this.game.getServerScore() < GameConstants.MAX_VALUE)
                sendGain(false);
            else if (this.game.getPlayerScore() < GameConstants.MAX_VALUE && this.game.getServerScore() > GameConstants.MAX_VALUE)
                sendGain(true);
            else
                if (this.game.getServerScore() > this.game.getPlayerScore())
                    sendGain(false);
                else if (this.game.getServerScore() < this.game.getPlayerScore())
                    sendGain(true);
                else{
                    this.game.setBet(0); // Draw
                    sendGain(true);
                }
    }
    
    private void sendGain(boolean win) throws IOException {
        this.com.writeID(GameConstants.GAINS, true);
        System.out.print(GameConstants.GAINS+" ");
        if (win){
            this.com.write_int32(this.game.getBet()*2);
            System.out.println(this.game.getBet()*2);
            this.com.write_log(this.game.getID(), "S: "+GameConstants.GAINS+" "+(this.game.getBet()*2));
        }else{
            this.com.write_int32(-this.game.getBet());
            System.out.println(-this.game.getBet());
            this.com.write_log(this.game.getID(), "S: "+GameConstants.GAINS+" "+this.game.getBet());
        }
    }
    
    /* #########################################################################
     * END SENDING METHODS
     * #########################################################################
     */
    
    
    /* #########################################################################
     * RECEIVING METHODS
     * #########################################################################
     */
    public String receiveID() throws IOException{
        return this.com.read_string();
    }
    
    public String receivePlayerMove() throws IOException{
        return this.com.read_string();
    }
    
    public int receiveAnte() throws IOException {
        this.com.read_bytes(1);
        return this.com.read_int32();
    }

    /* #########################################################################
     * END RECEIVING METHODS
     * #########################################################################
     */
    
    public void init_log(){
        this.com.init_log(this.game.getID());
    }
    
    public void write_log(String txt){
        this.com.write_log(this.game.getID(), txt);
    }
    
    public void close_log(){
        this.com.close_log();
    }
}
