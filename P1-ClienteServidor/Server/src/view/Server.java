package view;

import control.Control;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import model.GameConstants;

/**
 *
 * @author admin
 */
public class Server {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Control core;
        System.out.println("Iniciant el servidor de 7-1/2");
        
        try {
            System.out.println("IP: "+InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException ex) {
            System.err.println("Error amb la IP local");
        }
        
        if (args.length==0){
            System.out.println("Port: "+GameConstants.SERVER_PORT);
            core = new Control();
        }else if (args.length==1){
            System.out.println("Port: "+args[0]);
            core = new Control(args[0]);
        }else throw new IllegalArgumentException("Incorrect number of arguments.");
        
        System.out.println("Servidor Online ('end' per tancar):");        
        String end = new String();
        Scanner sc = new Scanner(System.in);
        core.start();
        while(!end.equals("end")) end = sc.nextLine();
        try {
            core.close();
        } catch (IOException ex) {
            System.err.println("Error tancant el servidor");
        }
    }
    
}