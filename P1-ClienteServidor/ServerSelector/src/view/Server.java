package view;

import control.GameThread;
import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author admin
 */
public class Server {

    	public static void main(String[] args) throws IOException {
                int n = 0;
		Selector selector = Selector.open();
		ServerSocketChannel server = ServerSocketChannel.open();
		server.socket().bind(new java.net.InetSocketAddress(1212));
		server.configureBlocking(false);
		server.register(selector, SelectionKey.OP_ACCEPT);
		while (true) {
			selector.select();
			Set keys = selector.selectedKeys();
                        System.out.println(keys.size());
			System.out.println("En el bucle");
			for (Iterator i = keys.iterator(); i.hasNext();) {
                                System.out.println("En el for");
				SelectionKey key = (SelectionKey) i.next();
				i.remove();
				
				
                                if (key.isAcceptable()) {
                                    System.out.println("Nuevo cliente, inicio partida");
                                    SocketChannel client = server.accept();
                                    client.configureBlocking(false);
                                    SelectionKey clientkey = client.register(selector, SelectionKey.OP_READ);

                                    // Creo juego nuevo
                                    GameThread g = new GameThread(n, client);
                                    clientkey.attach(g);
                                    n+=1;
                                }else {
                                        
                                    GameThread g = (GameThread)key.attachment();
                                    g.next_step();
                                    System.out.println("Jugando partida"+g.get_id());
                                    g.next_step();
                                    
				}
			}
		}
	}
    
}