package model;

/**
 *
 * @author admin
 */
public class Card {
    private String name;
    private double value;
    
    public Card(String card){
        this.name = card;
        if (this.name.toLowerCase().charAt(0) == 's' || this.name.toLowerCase().charAt(0) == 'c'
                || this.name.toLowerCase().charAt(0) == 'r')
            this.value = 0.5;
        else    this.value = Double.parseDouble(this.name.substring(0, 1));
    }
    
    public double getValue(){
        return this.value;
    }
    
    @Override
    public String toString(){
        return this.name;
    }
}
