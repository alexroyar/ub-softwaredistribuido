package model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author admin
 */
public class GameConstants {
    public static final int SERVER_PORT = 1212;
    
    public static final int ID_LENGTH = 4;
    
    public static final int DEFAULT_BET = 100;
    public static final double MAX_VALUE = 7.5;
    
    // Client Commands Semantics
    public static final String START = "strt";
    public static final String DRAW = "draw";
    public static final String ANTE = "ante";
    public static final String PASS = "pass";
    
    // Servers Commands Semantics
    public static final String STARTING_BET = "stbt";
    public static final String CARD = "card";
    public static final String BUSTING = "bstg";
    public static final String BANK_SCORE = "bksc";
    public static final String GAINS = "gain";
}
