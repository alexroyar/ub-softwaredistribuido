/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.ComUtils;
import control.Control;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author admin
 */
public class Client {
    private String serverIP;
    private int port;
    private InetAddress server;
    private Socket socket = null;
    private ComUtils utils;
    private Control ctrl;
    private ConcurrentHashMap<String, String> options = new ConcurrentHashMap<String, String>();
    
    
    public Client(String[] args) {
        this.options.put("-s","");
        this.options.put("-p","");
        this.options.put("-a","");
        
        if(args.length == 1 && args[0].equalsIgnoreCase("-h")){
            System.out.println("Help!");
        }else if(args.length % 2 != 0){
            System.out.println("Us: java Client <maquina_servidora> <port>");
        }else{
            for (int i = 0; i < args.length; i++) {
                if(this.options.containsKey(args[i])){
                    if(!this.options.containsKey(args[i+1])){
                        this.options.put(args[i],args[i+1]);
                        i+=1;
                    }
                    else{
                        System.out.println("Ussage: java Client <maquina_servidora> <port>");
                        System.exit(1);
                    }
                }
            }
        }
        if (this.options.containsValue("")){
            for (Map.Entry<String, String> entry : this.options.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value.equalsIgnoreCase(""))
                    this.options.remove(key, value);
            }
        }
        for (Map.Entry<String, String> entry : this.options.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key+","+value);
        }

        if(this.options.size() == 2){
            try{
                this.serverIP = this.options.get("-s");
                this.port     = Integer.parseInt(this.options.get("-p"));
                server = InetAddress.getByName(this.serverIP);
                socket = new Socket(server, this.port);

                this.utils = new ComUtils(socket);

                ctrl = new Control(this.utils);

                ctrl.start();
            }catch (IOException e){
                System.out.println("Error");
            }
            finally {
                try {
                    if (socket != null) socket.close();
                } catch (IOException e) {
                    System.out.println("Error");
                }
            }
        }else{
            try{
                float topcard = Float.valueOf(this.options.get("-a"));
                this.serverIP = this.options.get("-s");
                this.port     = Integer.parseInt(this.options.get("-p"));
                server = InetAddress.getByName(this.serverIP);
                socket = new Socket(server, this.port);

                this.utils = new ComUtils(socket);

                ctrl = new Control(this.utils);

                ctrl.start();
            }catch (IOException e){
                System.out.println("Error");
            }
            finally {
                try {
                    if (socket != null) socket.close();
                } catch (IOException e) {
                    System.out.println("Error");
                }
            }   
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new Client(args);
    }
    
}
