package control;

import java.io.IOException;
import java.util.Scanner;
import model.Card;
import model.Game;
import model.GameConstants;

/**
 *
 * @author admin
 */
public class Control {
    private Communication com;
    private Scanner sc;
    
    private Game game;
    
    private boolean finish;
    private boolean busting;

    public Control(ComUtils c) throws IOException{
        this.com = new Communication(c);
        this.sc = new Scanner(System.in);
        this.game = new Game();
        this.finish = false;
        this.busting = false;
    }
    
    public void start() throws IOException{
        this.com.startGame();
        System.out.println("start game");
        receive();
        int in;
        while (!finish){
            printMenu();
            in = Integer.parseInt(sc.nextLine());
            switch(in){
                case 1: // DRAW
                    this.com.draw();
                    break;
                case 2: // ANTE
                    System.out.println("How much?");
                    in = Integer.parseInt(sc.nextLine());
                    this.com.ante(in);
                    this.com.draw();
                    break;
                case 3: // PASS
                    this.com.pass();
                    finish=true;
            }
            receive();  
        }
        if (busting)
            receive(); // receive busting, bank score and gains
    }
    
    /**
     * this method is used to receive data from server
     * @throws IOException 
     */
    public void receive() throws IOException{
        String id = this.com.readId();
        System.out.print(id);
        switch(id.toLowerCase()) {
            case GameConstants.STARTING_BET:
                System.out.println(" "+this.com.readBet());
                break;
            case GameConstants.CARD:
                this.game.getPlayerCards().addLast(this.com.readCard());
                System.out.println(" "+this.game.getPlayerCards().getLast().toString());
                if (this.game.getPlayerScore() > GameConstants.MAX_VALUE){
                    finish = true;
                    busting = true;
                }
                break;
            case GameConstants.BUSTING:
                this.com.readBusting();
                System.out.println("");
                this.busting= true;
                break;
            case GameConstants.BANK_SCORE:
                this.com.readBankScore();
                break;
            /*default:
                System.out.println("Unknow ID");
                break;
            */
        }
    }
    
    public void printMenu(){
        System.out.println("LIST OF PLAYER CARDS");
        for (Card c:this.game.getPlayerCards()){
            System.out.print(c+ " ");
        }
        System.out.println("\nPLAYER SCORE: "+this.game.getPlayerScore());
        System.out.println("############## MENU ################");
        System.out.println("1. DRAW");
        System.out.println("2. ANTE");
        System.out.println("3. PASS");
    }
}
