import java.io.IOException;

/**
 *
 * @author admin
 */
public class Communication {
    private ComUtils con;
    
    public Communication(ComUtils c) throws IOException {
        this.con = c;
    }
    /* #########################################################################
     * SENDING METHODS
     * #########################################################################
     */
    
    
    /**
     * This method is used to start a game with the server.
     * @throws IOException 
     */
    public void startGame() throws IOException{
        con.writeID(GameConstants.START, false);
        //con.write_string(GameConstants.START);
    }
    
    /**
     * This method is used to ask server for a new card
     * @throws IOException 
     */
    public void draw() throws IOException{
        con.writeID(GameConstants.DRAW, false);
        //con.write_string(GameConstants.DRAW);
    }
    
    /**
     * This method is used to increase the bet of the game.
     * @param value
     * @throws IOException 
     */
    public void ante(int value) throws IOException{
        con.writeID(GameConstants.ANTE, true);
        //con.write_string(GameConstants.ANTE + " ");
        con.write_int32(value);
    }
    
    /**
     * This method passes the turn to the bank.
     * @throws IOException 
     */
    public void pass() throws IOException{
        con.writeID(GameConstants.PASS, false);
        //con.write_string(GameConstants.PASS);
    }
    
    /* #########################################################################
     * END SENDING METHODS
     * #########################################################################
     */

    
    /* #########################################################################
     * RECEIVING METHODS
     * #########################################################################
     */
    public String readId() throws IOException{
        return this.con.read_string();
    }
    
    public int readBet() throws IOException{
        this.con.read_bytes(1);
        return this.con.read_int32();
    }
    
    public Card readCard() throws IOException{
        con.read_bytes(1); // read the white space
        String str = "";
        byte card[] = con.read_bytes(2);
        for (int i = 0; i < card.length; i++) {
            str += (char)card[i];
        }
        return new Card(str);
    }
    
    /**
     * Reads the bank cards on the BANKSCORE set. It reads 1 card without the white
     * space.
     * @return
     * @throws IOException 
     */
    public Card readCards() throws IOException{
        String str = "";
        byte card[] = con.read_bytes(2);
        for (int i = 0; i < card.length; i++) {
            str += (char)card[i];
        }
        return new Card(str);
    }
    
    public void readBusting() throws IOException{
        System.out.print("\n"+GameConstants.BANK_SCORE+" ");
        if (readId().toLowerCase().equals(GameConstants.BANK_SCORE))
            readBankScore();
    }
    
    public void readBankScore() throws IOException{
        this.con.read_bytes(1);
        int num = con.read_int32();
        String cards = new String();
        cards += " "+num;
        for (int i=0; i<num;i++)
            cards+= readCards().toString();
        this.con.read_bytes(1); // white space before server points
        byte p[] = this.con.read_bytes(4); // the 2 numbers previous to the point + point + 1 decimal
        cards += " ";
        for (byte b:p)
            cards += (char)b;
        System.out.println(cards);
        int gain = readGains();
        if (gain == -1) System.out.println("ERROR");
        else if (gain<0)    System.out.println("YOU LOSE");
        else if (gain>0)    System.out.println("YOU WIN");
        else    System.out.println("DRAW");
    }
    
    public int readGains() throws IOException{
        if (con.read_string().toLowerCase().equals(GameConstants.GAINS)){
            this.con.read_bytes(1);
            int bet = con.read_int32();
            System.out.println(GameConstants.GAINS+" "+bet);
            return bet;
        }
        return -1;
    }

    /* #########################################################################
     * END RECEIVING METHODS
     * #########################################################################
     */
}
