

import java.util.LinkedList;

/**
 *
 * @author admin
 */
public class Game {
    private LinkedList<Card> cards;
    private LinkedList<Card> playerCards;
    private LinkedList<Card> serverCards;
    
    private int bet;
    
    public Game() {
        this.cards = new LinkedList<Card>();
        this.playerCards = new LinkedList<Card>();
        this.serverCards = new LinkedList<Card>();
        
        this.bet = GameConstants.DEFAULT_BET;
    }
    
    /*
     * #############################################################
     *   CARDS RELATED FUNCTIONS
     * #############################################################
    */
    
    public void readCards(){
        
    }
    
    public LinkedList<Card> getCards(){
        return this.cards;
    }
    
    public LinkedList<Card> getPlayerCards(){
        return this.playerCards;
    }
    
    public LinkedList<Card> getServerCards(){
        return this.serverCards;
    }
    
    private void addCard(LinkedList<Card> list){
        list.addLast(this.cards.getFirst());
        this.cards.removeFirst();
    }
    
    public Card addPlayerCard(){
        addCard(this.playerCards);
        return this.playerCards.getLast();
    }
    
    public void addServerCard(){
        addCard(this.serverCards);
    }
    
    /*
     * #############################################################
     *   END CARDS RELATED FUNCTIONS
     * #############################################################
    */
    /*
     * #############################################################
     *   BET RELATED FUNCTIONS
     * #############################################################
    */
    
    public void increaseBet(int value){
        this.bet += value;
    }
    
    public int getBet(){
        return this.bet;
    }
    
    /*
     * #############################################################
     *   END BET RELATED FUNCTIONS
     * #############################################################
    */
    
    private double getScore(LinkedList<Card> list){
        double score = 0;
        for (Card c:list)
            score += c.getValue();
        return score;
    }
    
    public double getPlayerScore(){
        return this.getScore(this.playerCards);
    }
    
    public double getServerScore(){
        return this.getScore(this.serverCards);
    }
}