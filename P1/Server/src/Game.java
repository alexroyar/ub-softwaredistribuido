

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author admin
 */
public class Game {
    private LinkedList<Card> cards;
    private LinkedList<Card> playerCards;
    private LinkedList<Card> serverCards;
    
    private int bet;
    private int id;
    
    public Game() {
        this.cards = new LinkedList<Card>();
        this.playerCards = new LinkedList<Card>();
        this.serverCards = new LinkedList<Card>();
        
        this.bet = GameConstants.DEFAULT_BET;
    }
    
    /*
     * #############################################################
     *   CARDS RELATED FUNCTIONS
     * #############################################################
    */
    
    /**
     * 
     * 
     * @param file
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void readCards(String file) throws FileNotFoundException, IOException{
        ArrayList<String> tmp = FileManager.readFile(file);
        
        for (String s:tmp)
            this.cards.addLast(new Card(s));
    }
    
    public LinkedList<Card> getCards(){
        return this.cards;
    }
    
    public LinkedList<Card> getPlayerCards(){
        return this.playerCards;
    }
    
    public LinkedList<Card> getServerCards(){
        return this.serverCards;
    }
    
    /**
     * Add the top card of the deck to the users cards and then remove it from the main deck.
     * @param list 
     */
    private void addCard(LinkedList<Card> list){
        list.addLast(this.cards.getFirst());
        this.cards.removeFirst();
    }
    
    public Card addPlayerCard(){
        addCard(this.playerCards);
        return this.playerCards.getLast();
    }
    
    public void addServerCard(){
        addCard(this.serverCards);
    }
    
    /*
     * #############################################################
     *   END CARDS RELATED FUNCTIONS
     * #############################################################
    */
    /*
     * #############################################################
     *   BET RELATED FUNCTIONS
     * #############################################################
    */
    
    public void increaseBet(int value){
        this.bet += value;
    }
    
    public int getBet(){
        return this.bet;
    }
    
    public void setBet(int value){
        this.bet = value;
    }
    
    /*
     * #############################################################
     *   END BET RELATED FUNCTIONS
     * #############################################################
    */
    
    private double getScore(LinkedList<Card> list){
        double score = 0;
        for (Card c:list)
            score += c.getValue();
        return score;
    }
    
    public double getPlayerScore(){
        return this.getScore(this.playerCards);
    }
    
    public double getServerScore(){
        return this.getScore(this.serverCards);
    }
    
    public void serverPlay(){
        if (getPlayerScore() > GameConstants.MAX_VALUE)
            addServerCard();
        else
            while (getServerScore() <= getPlayerScore())
                addServerCard();
    }
    
    public int getID(){
        return this.id;
    }
}