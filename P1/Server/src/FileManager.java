

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class FileManager {
    public static ArrayList<String> readFile(String file) throws FileNotFoundException, IOException{
        ArrayList<String> tmp = new ArrayList<>();        
        BufferedReader br = new BufferedReader(new FileReader(file));
        
        try {
            String line = br.readLine();

            while (line != null) {
                tmp.add(line);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return tmp;
    }
}
