

import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class Control extends Thread{
    public static enum CoreState {RUNNING, STOP};
    private CoreState state;
    
    private ServerSocket server;
    private LinkedList<GameThread> game_list;
    private int id_count;
    
    /**
     * Constructor of class Core. It Initialises the necessary variables.
     */
    public Control() {
        super();
        try {
            this.server = new ServerSocket(GameConstants.SERVER_PORT);
            init();
        } catch (IOException ex) {
            System.err.println("Connexió amb el port "+GameConstants.SERVER_PORT+
                    " no suportada.");
        }
    }
    
    /**
     * Constructor of class Core. It Initialises the necessary variables.
     * @param port The port that will use the server.
     */
    public Control(String port) {
        super();
        try {
            this.server = new ServerSocket(Integer.parseInt(port));
            init();
        } catch (IOException ex) {
            System.err.println("Connexió amb el port "+port+
                    " no suportada.");
        }
    }
    
    public void init(){
        this.game_list = new LinkedList<GameThread>();
        this.id_count = 0;
        this.state = CoreState.RUNNING;
        System.out.println("Core creat");
    }
    
    @Override
    public void run() {
        wait_for_clients();
    }
    
    public void finalize(){
        this.state = CoreState.STOP;
    }
    
    /**
     * Method to wait for clients.
     */
    private void wait_for_clients() {
        
        
        /*
        this.game_list.add(new GameThread(this.id_count++,this.server));
        this.game_list.getLast().start();
        while(this.state.equals(CoreState.RUNNING)) {
            if (this.game_list != null && this.game_list.size()>0 && this.game_list.getLast().signal() == true) {
                System.out.println("Creating a new game with ID: "+(this.id_count-1));
                System.out.println("--------------------------------------------");
                this.game_list.add(new GameThread(this.id_count++, this.server));
                this.game_list.getLast().start();
            }
            checkGamesStatus();
        }
        */
    }
    
    /**
     * Checks the games and removes the games that are finished (Naturally form or
     * for error).
     */
    private void checkGamesStatus() {
        for (GameThread gt:this.game_list)
            if (gt.isEnded()){
                //System.out.println("Removing game with ID: "+gt.getId());
                this.game_list.remove(gt);
            }
    }
    
    /**
     * Closes the connection of the server and ends the core.
     * @throws IOException 
     */
    public void close() throws IOException {
        this.server.close();
        this.game_list.clear();
    }
}
