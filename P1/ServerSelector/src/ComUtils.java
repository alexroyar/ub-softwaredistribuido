

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
 
public class ComUtils {
    /* Mida d'una cadena de caracters */
    private final int STRSIZE = 40;
    private SocketChannel socket;
    /* Objectes per escriure i llegir dades */
    private DataInputStream dis;
    //private DataOutputStream dos;

    public ComUtils(SocketChannel socket) throws IOException {
        this.socket=socket;
        //dis = new DataInputStream(socket.getInputStream());
        //dos = new DataOutputStream(socket.getOutputStream());
    }

    /* Llegir un enter de 32 bits */
    public int read_int32() throws IOException {
        byte bytes[] = new byte[4];
        bytes = read_bytes(4);

        return bytesToInt32(bytes,"be");
    }

    /* Escriure un enter de 32 bits */
    public void write_int32(int number) throws IOException {
        
        byte bytes[]=new byte[4];        

        int32ToBytes(number,bytes,"be");
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        this.socket.write(buffer);
    }

    /* Llegir un string de mida STRSIZE */
    public String read_string() throws IOException {
        byte read[] = read_bytes(4);
        String toRet = new String();
        for (int i = 0; i < read.length; i++) {
            toRet += (char)read[i];
        }
        return toRet;
    }

    /* Escriure un string */
    public void write_string(String str) throws IOException {
        byte[] b = new byte[str.length()];
        for (int i = 0; i < str.length(); i++) {
            b[i] = (byte)str.charAt(i);
        }
        ByteBuffer buffer = ByteBuffer.wrap(b);
        socket.write(buffer);
    }
    
    

    /* Passar d'enters a bytes */
    private int int32ToBytes(int number,byte bytes[], String endianess) {
        if("be".equals(endianess.toLowerCase())) {
            bytes[0] = (byte)((number >> 24) & 0xFF);
            bytes[1] = (byte)((number >> 16) & 0xFF);
            bytes[2] = (byte)((number >> 8) & 0xFF);
            bytes[3] = (byte)(number & 0xFF);
        } else {
            bytes[0] = (byte)(number & 0xFF);
            bytes[1] = (byte)((number >> 8) & 0xFF);
            bytes[2] = (byte)((number >> 16) & 0xFF);
            bytes[3] = (byte)((number >> 24) & 0xFF);
        }
        return 4;
    }

    /* Passar de bytes a enters */
    private int bytesToInt32(byte bytes[], String endianess) {
        int number;

        if("be".equals(endianess.toLowerCase())) {
            number=((bytes[0] & 0xFF) << 24) | ((bytes[1] & 0xFF) << 16) |
            ((bytes[2] & 0xFF) << 8) | (bytes[3] & 0xFF);
        } else {
            number=(bytes[0] & 0xFF) | ((bytes[1] & 0xFF) << 8) |
            ((bytes[2] & 0xFF) << 16) | ((bytes[3] & 0xFF) << 24);
        }
        return number;
    }
    
    public void writeCard(Card c) throws IOException {
        char string[] = c.toString().toCharArray();
        byte send[] = new byte[string.length];
        for (int i=0; i<string.length;i++)
            send[i] = (byte) string[i];
            
        write_bytes(send);
    }
    
    public void write_bytes(byte b[]) throws IOException {
        ByteBuffer buffer = ByteBuffer.wrap(b);
        socket.write(buffer);
    }
    
    public void writeID(String id, boolean space) throws IOException{
        byte[] b;
        if (space){
           b = new byte[id.length()+1]; 
        }else{
           b = new byte[id.length()]; 
        }
            
        for (int i = 0; i < id.length(); i++) {
            b[i] = (byte)id.charAt(i);
        }
        if (space)
            b[id.length()] = (byte)' ';
        
        ByteBuffer buffer = ByteBuffer.wrap(b);
        this.socket.write(buffer);
    }
    
    //llegir bytes.
    public byte[] read_bytes(int numBytes) throws IOException{
        System.out.println(numBytes);
        ByteBuffer buffer = ByteBuffer.allocate(numBytes);
        buffer.flip();
        buffer.clear();
        this.socket.read(buffer);
        System.out.println("Recibo "+buffer.array().length+" chars");
        
        if (buffer.array().length == numBytes){
            return buffer.array();
        }else{
            return null;
        }
        /*int len=0 ;
        byte bStr[] = new byte[numBytes];
        int bytesread=0;
        do {
            bytesread= dis.read(bStr, len, numBytes-len);
            if (bytesread == -1)
                throw new IOException("Broken Pipe");
            len += bytesread;
        } while (len < numBytes);
        return bStr;*/
    }
    /* Llegir un string mida variable size = nombre de bytes especifica la longitud*/
    public String read_string_variable(int size) throws IOException {
        byte bHeader[]=new byte[size];
        char cHeader[]=new char[size];
        int numBytes=0;
        // Llegim els bytes que indiquen la mida de l'string
        bHeader = read_bytes(size);
        // La mida de l'string ve en format text, per tant creem un string i el parsejem
        for(int i=0;i<size;i++){
            cHeader[i]=(char)bHeader[i]; }
        numBytes=Integer.parseInt(new String(cHeader));
        // Llegim l'string
        byte bStr[]=new byte[numBytes];
        char cStr[]=new char[numBytes];
        bStr = read_bytes(numBytes);
        for(int i=0;i<numBytes;i++)
            cStr[i]=(char)bStr[i];
        return String.valueOf(cStr);
    }
    /* Escriure un string mida variable, size = nombre de bytes especifica la longitud */
    /* String str = string a escriure.*/
    public void write_string_variable(int size,String str) throws IOException {
        // Creem una seqüència amb la mida
        byte bHeader[]=new byte[size];
        String strHeader;
        int numBytes=0;
        // Creem la capçalera amb el nombre de bytes que codifiquen la mida
        numBytes=str.length();
        strHeader=String.valueOf(numBytes);
        int len;
        if ((len=strHeader.length()) < size)
        for (int i =len; i< size;i++){
            strHeader= "0"+strHeader;}
        for(int i=0;i<size;i++)
            bHeader[i]=(byte)strHeader.charAt(i);
        // Enviem la capçalera
        //dos.write(bHeader, 0, size);
        // Enviem l'string writeBytes de DataOutputStrem no envia el byte més alt dels chars.
        //dos.writeBytes(str);
    }
} 