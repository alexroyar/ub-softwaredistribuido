

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SocketChannel;

/**
 *
 * @author Juan Manuel Morales Garzón
 */
public class GameThread{
    private SocketChannel server;
    private Communication com;
    private Game game;

    private int id;
    private String in;
    private boolean ended = false;
    private boolean finish = false;
    private boolean signal = false;
    
    private int status = -1;

    /**
     * Constructor of class GameThread. It initialises the lists and variables
     * that will be used on the game.
     * @param id the id of the game.
     * @param socket the socket that will be used.
     */
    public GameThread(int id, SocketChannel socket) throws IOException {
        this.id = id;
        this.server = socket;
        this.init();
    }
    
    public int get_id(){return this.id;}
    
    public Communication getCom(){return this.com;}
    
    public boolean checkBusting(){return this.com.checkBusting();}
    
    /**
     * Initializes the GameThread to start a new game.
     * @param socket
     * @throws IOException 
     */
    private void init() throws IOException {
        this.game = new Game();
        System.out.println("read cards");
        this.game.readCards("src/Resources/deck.txt");
        System.out.println("BEGIN PROTOCOL");
        
        this.signal = true;
        
        this.com = new Communication(this.server, this.game);
    }
    
    /*public void read_step() throws IOException{
        System.out.println("GameThread: estoy en read_step()");
        in = com.receivePlayerMove();
        System.out.println("in:\t"+in);
        switch (in){
            case GameConstants.START:
                this.status = GameConstants.START_GAME_STATUS;
                break;
            case GameConstants.ANTE:
                int v = com.receiveAnte();
                this.game.increaseBet(v);
                System.out.println(" "+v);
                read_step();
                break;
            case GameConstants.DRAW:
                this.status = GameConstants.DRAW_STATUS;
                break;
            case GameConstants.PASS:
                this.status = GameConstants.PASS_STATUS;
                break;
        }
    }
    
    public void next_step() throws IOException{
        System.out.println("GameThread: estoy en next_step()");
        if(this.status == GameConstants.INITIAL_STATUS){
            read_step();
            com.sendBet();
            //next_step();
        }else{
            switch (this.status){
                case GameConstants.START_GAME_STATUS:
                    com.sendBet();
                    read_step();
                    break;
                case GameConstants.DRAW_STATUS:
                    com.sendCard();
                    read_step();
                    break;
                case GameConstants.PASS_STATUS:
                    com.sendBankScore();
                    break;
            }
        }
    }*/
   
    public void setMove(String received) throws IOException{
        switch(received.toLowerCase()){
            case GameConstants.START:
                com.sendBet();
                break;
            case GameConstants.DRAW:
                com.sendCard();
                break;
            case GameConstants.ANTE:
                int ante = com.receiveAnte();
                this.game.increaseBet(ante);
                System.out.println("ante "+ante);
                break;
            case GameConstants.PASS:
                com.sendBankScore();
                break;
        }
    }
    
    /* ===================================================
     * PLAYING METHODS
     * ===================================================
     */
    /**
     * returns the signal that marks this game as an active and not finished
     * @return signal
     */
    public boolean signal(){
        return signal;
    }
 
    /* ===================================================
     * END PLAYING METHODS
     * ===================================================
     */
    
    /**
     * return a boolean variable that indicate that the game has ended.
     * @return 
     */
    public boolean isEnded(){
        return this.ended;
    }
    /**
     * Ends the connection with the client.
     * @throws IOException 
     */
    public void end() throws IOException {
    }
    
}
