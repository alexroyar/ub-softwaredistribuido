import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Set;

public class Servidor {
    public static void main(String[] args) throws IOException {
        int gameID = 0;
        Charset charset = Charset.forName("ISO-8859-1");
        CharsetEncoder encoder = charset.newEncoder();
        CharsetDecoder decoder = charset.newDecoder();
        ByteBuffer buffer = ByteBuffer.allocate(512);
        Selector selector = Selector.open();
        ServerSocketChannel server = ServerSocketChannel.open();
        server.socket().bind(new java.net.InetSocketAddress(1212));
        server.configureBlocking(false);
        SelectionKey serverkey = server.register(selector, SelectionKey.OP_ACCEPT);
        System.out.println("Server ready!");
        for (;;) {
            selector.select();
            Set keys = selector.selectedKeys();
            //System.out.println(keys.size());

            for (Iterator i = keys.iterator(); i.hasNext();) {
                SelectionKey key = (SelectionKey) i.next();
                i.remove();

                if (key == serverkey) { // first iteration, only new games.
                    if (key.isAcceptable()) {
                        System.out.println("Nuevo cliente, inicio partida");
                        SocketChannel client = server.accept();
                        client.configureBlocking(false);
                        SelectionKey clientkey = client.register(selector, SelectionKey.OP_READ);
                        
                        // Creo juego nuevo
                        GameThread g = new GameThread(gameID, client);
                        clientkey.attach(g);
                        gameID+=1;
                    }
                } else {
                    SocketChannel client = (SocketChannel) key.channel();

                    if (!key.isReadable())
                        continue;
                    
                    try{
                        GameThread g = (GameThread)key.attachment();
                        int actualID = g.get_id();
                        Communication c = g.getCom();
                        String request = c.receivePlayerMove();
                        System.out.println("request is: "+request);
                        if (request.trim().toLowerCase().equals(GameConstants.PASS) ||
                                request.trim().toUpperCase().equals(GameConstants.BUSTING)){
                            String response = actualID + ":: " + request.toUpperCase();
                            System.out.println(response);
                            g.setMove(request.trim());
                            key.cancel();
                            client.close();
                            System.out.println("Connexio tancada amb: "+actualID);
                        } else {
                            String response = actualID + ": " + request.trim().toUpperCase();
                            System.out.println(response);
                            g.setMove(request.trim());
                            if (g.checkBusting()){
                                key.cancel();
                                client.close();
                                System.out.println("Connexio tancada amb: "+actualID);
                            }else
                                key.attach(g);
                        }
                    }catch (IOException e){
                        System.err.println("Hi ha hagut algun problema de conexio");
                        key.cancel();
                        client.close();
                    }
                }
            }
        }
    }
}

