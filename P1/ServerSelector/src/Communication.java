

import java.io.IOException;
import java.nio.channels.SocketChannel;

/**
 *
 * @author admin
 */
public class Communication {
    private ComUtils com;
    private Game game;
    
    public Communication(SocketChannel sckt, Game game) throws IOException{
        this.com = new ComUtils(sckt);
        this.game = game;
    }
    
    public boolean checkBusting(){
        if (this.game.getPlayerScore() > GameConstants.MAX_VALUE)
            return true;
        else    return false;
    }
    
    /* #########################################################################
     * SENDING METHODS
     * #########################################################################
     */
    
    public void sendBet() throws IOException{
        this.com.writeID(GameConstants.STARTING_BET, true);
        this.com.write_int32(this.game.getBet());
        System.out.println(GameConstants.STARTING_BET+ " " + this.game.getBet());
    }
    
    public void sendCard() throws IOException{
        /*this.com.write_string(GameConstants.CARD+" ");
        this.com.write_string(this.game.addPlayerCard().toString());
        System.out.println(" "+this.game.getPlayerCards().getLast().toString());
        if (this.game.getPlayerScore() > GameConstants.MAX_VALUE)
            sendBusting();*/
        this.com.writeID(GameConstants.CARD, true);
        this.com.writeCard(this.game.addPlayerCard());
        System.out.println(GameConstants.CARD + " "+ this.game.getPlayerCards().getLast().toString());
        if (checkBusting())
            sendBusting();
    }
    
    public void sendBusting() throws IOException{
        this.com.writeID(GameConstants.BUSTING, false);
        System.out.println(GameConstants.BUSTING);
        sendBankScore();
    }
    
    public void sendBankScore() throws IOException{
        this.com.writeID(GameConstants.BANK_SCORE, true);
        this.game.serverPlay();
        this.com.write_int32(this.game.getServerCards().size());
        System.out.print(GameConstants.BANK_SCORE+" "+this.game.getServerCards().size());
        for (Card card:this.game.getServerCards()){
            this.com.writeCard(card);
            System.out.print(card.toString());
        }
        int i = 0;
        byte[] b = new byte[5];
        b[i] = (byte) ' ';i+=1;
        if (this.game.getServerScore() <= 10.0){
            b[i] = (byte)'0';i+=1;
        }
        String score = String.valueOf(this.game.getServerScore());
        for (int j = 0; j < score.length(); j++) {
            b[i] = (byte)score.charAt(j);
            i+=1;
        }
        this.com.write_bytes(b);
        
               System.out.println(" "+ this.game.getServerScore());
        if (this.game.getPlayerScore() > GameConstants.MAX_VALUE)
            sendGain(false);
        else
            if (this.game.getPlayerScore() > GameConstants.MAX_VALUE && this.game.getServerScore() < GameConstants.MAX_VALUE)
                sendGain(false);
            else if (this.game.getPlayerScore() < GameConstants.MAX_VALUE && this.game.getServerScore() > GameConstants.MAX_VALUE)
                sendGain(true);
            else
                if (this.game.getServerScore() > this.game.getPlayerScore())
                    sendGain(false);
                else if (this.game.getServerScore() < this.game.getPlayerScore())
                    sendGain(true);
                else{
                    this.game.setBet(0); // Draw
                    sendGain(true);
                }
    }
    
    private void sendGain(boolean win) throws IOException {
        this.com.writeID(GameConstants.GAINS, true);
        System.out.print(GameConstants.GAINS+" ");
        if (win){
            this.com.write_int32(this.game.getBet()*2);
            System.out.println(this.game.getBet()*2);
        }else{
            this.com.write_int32(-this.game.getBet());
            System.out.println(-this.game.getBet());
        }
    }
    
    /* #########################################################################
     * END SENDING METHODS
     * #########################################################################
     */
    
    
    /* #########################################################################
     * RECEIVING METHODS
     * #########################################################################
     */
    
    public String receivePlayerMove() throws IOException{
        return this.com.read_string();
    }
    
    public int receiveAnte() throws IOException {
        this.com.read_bytes(1);
        return this.com.read_int32();
    }

    /* #########################################################################
     * END RECEIVING METHODS
     * #########################################################################
     */
}
