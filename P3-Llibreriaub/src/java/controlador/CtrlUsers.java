/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import modelo.User;

/**
 *
 * @author alex
 */
public class CtrlUsers extends ConcurrentHashMap<String, User> implements Serializable{
       
    public void add(User user){
        
        if(!containsKey(user.getName()))super.put(user.getName(), user);
    }
    
    public void remove(String name){
        if (containsKey(name)) super.remove(name);
    }
    
    public void remove(User user){
        if (containsKey(user.getName())) super.remove(user.getName());
    }
    
    public User get(String string) {
        return super.get(string);
    }
    
    public User login(String user){
        User u = null;
        
        if(!containsKey(user)){
            u = new User(user);
            super.put(user, u);
        }else{
            u = super.get(user);
        }
        return u;
    }
    
    public void saveUsers(String path) throws FileNotFoundException, IOException{
        FileOutputStream f_out = new FileOutputStream(path);
        ObjectOutputStream obj_out = new ObjectOutputStream (f_out);      
        obj_out.writeObject ( this );
    }
    public void loadUsers(String path) throws FileNotFoundException, IOException, ClassNotFoundException{
        FileInputStream f_out = new FileInputStream(path);
        ObjectInputStream obj_in = new ObjectInputStream (f_out);
        Object tmp = obj_in.readObject();
        if(tmp instanceof CtrlUsers){
            CtrlUsers users_tmp = (CtrlUsers) tmp;
            for(Entry<String, User> u : users_tmp.entrySet()){
                put(u.getKey(), u.getValue());
            }
        }
        

    }
}
