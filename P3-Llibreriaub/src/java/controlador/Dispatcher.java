/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Resource;
import modelo.User;

/**
 *
 * @author web
 */

public class Dispatcher extends HttpServlet  {

    private CtrlUsers users;
    private ArrayList<Resource> resources;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.resources = new ArrayList<>();
        this.users = new CtrlUsers();
        try {
            this.users.loadUsers("data.dat");
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Error cargando datos");
        }
        load_resources();
    }
    
    private void load_resources(){
        String xml_path = this.getServletContext().getRealPath("/WEB-INF/items/ResourceData.xml");
        log(xml_path);
        this.resources = utis.Utils.get_resources_from_xml(xml_path,this.getServletContext());
        log("Recursos: "+this.resources.size());
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        client_identifier(request);
        String uri = request.getRequestURI();
        if(!uri.contains("/API/")){
            switch(uri){
                case "/llibreria/":
                    goToHome(request, response);
                    break;
                case "/llibreria/cataleg":
                    goToCataleg(request, response);
                    break;
                case "/llibreria/cataleg/llibres":
                    goToLlibres(request, response);
                    break;
                case "/llibreria/cataleg/videos":
                    goToVideos(request, response);
                    break;
                case "/llibreria/cataleg/audios":
                    goToAudios(request, response);
                    break;
                case "/llibreria/protegit/llista":
                    goToLlista(request, response);
                    break; 
            }
        }else if(uri.contains("/API/")){
            if(uri.contains("/API/AUDIO/cataleg")){
                //TODO Return json
            }else if(uri.contains("/API/VIDEO/cataleg")){
                //TODO Return json
            }else if(uri.contains("/API/BOOK/cataleg")){
                //TODO Return json
            }else if(uri.contains("/API/BOOK/item/")){
                String item = uri.split("/")[uri.split("/").length-1];
                //TODO Return json
            }
        }
    }
            
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        String action = request.getParameter("action");
        switch(request.getRequestURI()){
            case "/llibreria/":
                switch(action){
                    case "buy":
                        buy_resource(request);
                        break;
                    case "cart":
                        add_cart_resource(request);
                        break;
                    case "logout":
                        request.getSession().invalidate();
                        break;
                }
                response.sendRedirect(request.getRequestURI());
                break;
            case "/llibreria/cataleg":
                switch(action){
                    case "buy":
                        buy_resource(request);
                        break;
                    case "cart":
                        add_cart_resource(request);
                        break;
                }
                response.sendRedirect(request.getRequestURI());
                break;
            case "/llibreria/cataleg/audios":
                switch(action){
                    case "buy":
                        buy_resource(request);
                        break;
                    case "cart":
                        add_cart_resource(request);
                        break;
                    case "logout":
                        request.getSession().invalidate();
                        break;
                }
                response.sendRedirect(request.getRequestURI());
                break;
            case "/llibreria/cataleg/videos":
                switch(action){
                    case "buy":
                        buy_resource(request);
                        break;
                    case "cart":
                        add_cart_resource(request);
                        break;
                    case "logout":
                        request.getSession().invalidate();
                        break;
                }
                response.sendRedirect(request.getRequestURI());
                break;
            case "/llibreria/cataleg/llibres":
                switch(action){
                    case "buy":
                        buy_resource(request);
                        break;
                    case "cart":
                        add_cart_resource(request);
                        break;                    
                    case "logout":
                        request.getSession().invalidate();
                        break;
                }
                response.sendRedirect(request.getRequestURI());
                break;
            case "/llibreria/protegit/llista":
                switch(action){
                    case "download":
                        download_file(request, response);
                        return;
                    case "buy":
                        buy_resource(request);
                        break;                    
                    case "logout":
                        request.getSession().invalidate();
                        break;
                }
                response.sendRedirect(request.getRequestURI());
                break;
            case "/llibreria/protegit/login":
                response.sendRedirect("/llibreria/");
                break; 
        }
        this.users.saveUsers("data.dat");
        
    }
    private void download_file(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String down_path = request.getParameter("resource");
        
        File f = new File(down_path);
        
        ServletOutputStream output = response.getOutputStream();
        response.setContentLength((int)f.length());
        //response.setHeader("Content-Type", );
        response.setHeader("Content-Lenght",""+f.length());
        response.setHeader("Content-Disposition", "attachment; filename=\""+f.getName()+"\"");
        
        byte[] byteBuffer = new byte[16048];
        DataInputStream in = new DataInputStream(new FileInputStream(f));
        
        int len;
        while((len = in.read(byteBuffer))!=-1){
            output.write(byteBuffer,0,len);
        }
        in.close();
        output.close();
    }
    private void client_identifier(HttpServletRequest request){
        String username = request.getRemoteUser();
        User u;
        if(username!=null){
            log("Está logueado el usuario "+username);
            User logued = this.users.login(username);
            u = (User) request.getSession(true).getAttribute("user");
            if(u!=null && u.isGuest()){
                logued.updateShoppingCart(u.getShoppingCart());
            }
            request.getSession(true).setAttribute("user", logued);
        }else{
            log("No hay usuario logueado");
            u = (User) request.getSession(true).getAttribute("user");
            if(u==null){
                log("NO hay ninguna sesión iniciada");
                u = new User("Invitado");
                u.setGuest(true);
                request.getSession(true).setAttribute("user", u);   
            }
        }
    }

    private void add_cart_resource(HttpServletRequest request){
        User user = (User) request.getSession(true).getAttribute("user");
        boolean all_ok = true;
        if(request.getParameter("add_cart")!=null){
            String path = (String) request.getParameter("add_cart");
            for(Resource r: this.resources){
                if(r.getPath().equalsIgnoreCase(path)){
                    all_ok = user.addToCart(r);
                }    
            }
        }else if(request.getParameter("remove_cart")!=null){
            String path = (String) request.getParameter("remove_cart");
            for(Resource r: this.resources){
                if(r.getPath().equalsIgnoreCase(path)){
                    all_ok = user.removeFromCart(r);
                }    
            }
        }
    }
    private void buy_resource(HttpServletRequest request){
        User user = (User) request.getSession(true).getAttribute("user");
        if(!user.isGuest()){
            log("Estoy comprando un producto "+request.getParameter("buy_now"));
            if(request.getParameter("buy_now")!=null){
                String path = (String) request.getParameter("buy_now");
                log("Confirmo el producto es "+path);
                for(Resource r: this.resources){
                    if(r.getPath().equalsIgnoreCase(path))
                        user.resourcePurchase(r);
                }
            }
        }
    }
    
    private void goToHome(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        request.getSession(true).setAttribute("resources", this.resources);
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
        rd.forward(request, response);
    }
    
    private void goToCataleg(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        log("Voy a catalogo");
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/WEB-INF/jsp/catalogo.jsp");
        rd.forward(request, response);
    }
    
    private void goToLlibres(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/WEB-INF/jsp/llibres.jsp");
        rd.forward(request, response);
    }
    
    private void goToVideos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/WEB-INF/jsp/videos.jsp");
        rd.forward(request, response);
    }
    
    private void goToAudios(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/WEB-INF/jsp/audios.jsp");
        rd.forward(request, response);        
    }
    
    private void goToLlista(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String user = (request.getRemoteUser() != null) ? request.getRemoteUser() : null;
        this.users.login(user);
        ServletContext sc = getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher("/WEB-INF/jsp/protected/descargas.jsp");
        rd.forward(request, response);          
    }


}
