/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import utis.Utils.filetype;

/**
 *
 * @author alex
 */
public class Resource extends File implements Serializable{
    private double price;
    private String name;
    private String author;
    private filetype type;
    private String description;

    public Resource(String path) {
        super(path);
    }
    
    public Resource(String path,String name, double price) throws Exception {
        super(path);
        this.name = name;
        this.price = price;
        if(!exists()){
            throw new Exception("File not exist");
        }
    }
    
    public void setName(String name){
        this.name = name;
    }
    public void setPrice(double price){
        this.price = price;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public filetype getType() {
        return type;
    }

    public void setType(filetype type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getName(){
        return this.name;
    }
    
    public double getPrice(){
        return this.price;
    }
    
    public String getPath(){
        return super.getPath();
    }
    
    @Override
    public boolean equals(Object o){
        return ((Resource) o).getPath().equalsIgnoreCase(super.getPath());
    }
}
