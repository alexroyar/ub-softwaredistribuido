/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author alex
 */
public class User implements Serializable {
    private String name;
    private double money;
    private ArrayList<Resource> shopping_cart;
    private ArrayList<Resource> acquired;
    private boolean guest;
    private int id;
    
    public User(String name) { 
        this.name = name;
        this.shopping_cart = new ArrayList<Resource>();
        this.acquired = new ArrayList<Resource>();
        this.money = 100.00;
        this.guest = false;
    }
    
    public void setGuest(boolean guest){
        this.guest=guest;
    }
    public boolean isGuest(){
        return this.guest;
    }
    
    public int getAcquired(){
        return this.acquired.size();
    }
    public ArrayList<Resource> getAllAcquired(){
        return this.acquired;
    }
    public ArrayList<Resource> getShoppingCart(){
        return this.shopping_cart;
    }
    
    public void updateShoppingCart(ArrayList<Resource> list){
        for(Resource tmp:list){
            if(!this.shopping_cart.contains(tmp) && !this.acquired.contains(tmp))
                this.shopping_cart.add(tmp);
        }
    }
    
    public boolean addToCart(Resource r){
        if(!this.shopping_cart.contains(r) && !this.acquired.contains(r))
            return this.shopping_cart.add(r);
        else
            return false;
    }
    public boolean removeFromCart(Resource r){
        if(this.shopping_cart.contains(r))
            return this.shopping_cart.remove(r);
        else
            return false;
    }
    public void addToAcquired(Resource r){
        this.acquired.add(r);
    }
    
    public boolean resourcePurchase(Resource r){
        if (!this.shopping_cart.contains(r)) return false;
        if (this.acquired.contains(r)) return false;
        if (this.money >= r.getPrice()) {
            this.addToAcquired(r);
            this.money -= r.getPrice();
            this.shopping_cart.remove(r);
            return true;
        }
        return false;
    }

    
    public String getName(){
        return this.name;
    }
    public double getMoney(){
        return this.money;
    }
}
