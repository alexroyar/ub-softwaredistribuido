/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import modelo.Resource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author alex
 */
public class Utils {
    public enum filetype {AUDIO,VIDEO,BOOK}
    public static String[] audio_format = {"mp3"};
    public static String[] video_format = {"avi"};
    public static String[] book_format = {"pdf"};
    
    
    public static ArrayList<Resource> get_resources_from_xml(String xml_path,ServletContext context){
        try {
            ArrayList<Resource> resources = new ArrayList<>();
            
            
            File fXmlFile = new File(xml_path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            
            
            NodeList nList = doc.getElementsByTagName("resource");
            System.out.println(nList.getLength());
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String path = eElement.getElementsByTagName("path").item(0).getTextContent();
                    String real_path = context.getRealPath(path);
                    String autor = eElement.getElementsByTagName("author").item(0).getTextContent();
                    String price = eElement.getElementsByTagName("price").item(0).getTextContent();
                    String name = eElement.getElementsByTagName("name").item(0).getTextContent();
                    String description = eElement.getElementsByTagName("description").item(0).getTextContent();
                    
                    Resource r = new Resource(real_path, name, Double.parseDouble(price));
                    r.setAuthor(autor);
                    r.setDescription(description);
                    r.setType(getFileType(getExtension(real_path)));
                    resources.add(r);
                }
            }
            return resources;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.out.println("Error loading file");
        } catch (Exception ex) {
            System.out.println("Error loading file");
        }
        return null;
    }
    
    public static String getExtension(String path){
        String extension = "";
        int i = path.lastIndexOf('.');
        if (i >= 0) {
            extension = path.substring(i+1);
        }
        return extension;
    }
    
    public static filetype getFileType(String extension){
        for(String s:audio_format){
            if (s.equalsIgnoreCase(extension))
                return filetype.AUDIO;
        }
        for(String s:video_format){
            if (s.equalsIgnoreCase(extension))
                return filetype.VIDEO;
        }
        for(String s:book_format){
            if (s.equalsIgnoreCase(extension))
                return filetype.BOOK;
        }
        System.out.println("No deberia llegar nunca aquiiiii\n\n Nuncaaa\nNuncaaa\n");
        return null;
    }
    
    
}
