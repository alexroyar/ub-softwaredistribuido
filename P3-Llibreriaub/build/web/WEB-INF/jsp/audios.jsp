<%@page import="utis.Utils.filetype"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.Resource"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Librer�a UB</title>
    <link href="<%= pageContext.getServletContext().getContextPath() %>/static/css/bootstrap.min.css" rel="stylesheet">
    <link href="<%= pageContext.getServletContext().getContextPath() %>/static/css/style.css" rel="stylesheet">
    <script src="<%= pageContext.getServletContext().getContextPath() %>/static/js/jquery.min.js"></script>
    <script src="<%= pageContext.getServletContext().getContextPath() %>/static/js/bootstrap.min.js"></script>

    <%@page import="modelo.User"%>
    <%
        User user = (User) request.getSession(true).getAttribute("user");
        ArrayList<Resource> resources = (ArrayList<Resource>) request.getSession(true).getAttribute("resources");
        String name = user!=null? user.getName() :"Bienvenido invitado";
        String money = user!=null? String.valueOf(user.getMoney()):"~";
        String adquiridos = user!=null? String.valueOf(user.getAcquired()):"0";
        ArrayList<Resource> shopping_cart = user!=null? user.getShoppingCart():null;
    %>
  </head>

  <body>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Librer�a UB</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Librer�a UB</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/llibreria">Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Cat�logo<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                  <li><a href="/llibreria/cataleg/llibres">Libros</a></li>
                  <li><a href="/llibreria/cataleg/audios">Audios</a></li>
                  <li><a href="/llibreria/cataleg/videos">V�deos</a></li>
                  <li><a href="/llibreria/cataleg">Cat�logo completo</a></li>
              </ul>
            </li>
            <li><a href="http://ub-gei-sd.github.io/" target="_blank">M�s informaci�n</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                  <%=name %> <span class="caret">
                      
                  </span></a>
              <ul class="dropdown-menu" role="menu">
                   <% if(!user.isGuest()){%>
                    <li><a href="/llibreria/protegit/llista">Archivos adquiridos (<%=adquiridos%>)</a></li>
                    <li>
                        <form action = ""  method="POST" Style="margin:15px;text-align: center;">
                            <input type="hidden" name="action" value="logout"/>
                            <button type="submit" class="btn btn-primary">Cerrar sesi�n</button>
                        </form>
                    </li>
                  <%}else{%>
                    <li><a href="/llibreria/protegit/llista">Iniciar sesi�n</a></li>
                  <%}%>
                
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span><span class="caret"></span></a>
              <ul class="dropdown-menu col-md-12" role="menu" Style="width:500px;">
                <%if(shopping_cart.isEmpty()){%>
                        <li class="list-group-item">
                                <span class="badge">0 </span>Productos
                        </li>
                    <%}else{
                        for(Resource r : shopping_cart){
                            String name_form = "document."+r.getPath()+".submit()";
                        %>
                        <div class="col-md-12" Style="padding:10px;">
                            <form action = ""  method="POST">
                                <div class="col-md-6" Style="text-align:left;line-height: 30px;">
                                    <p><%= r.getName() %></p>
                                </div>
                                <div class="col-md-4" Style="text-align:right">
                                    <input type="hidden" name="action" value="buy"/>
                                    <input type="hidden" name="buy_now" value="<%= r.getPath() %>"/>
                                    <button type="submit" class="btn btn-primary">Compra inmediata</button>
                                </div>
                                <div class="col-md-1" Style="text-align:right;line-height: 30px;">
                                    <p><%= r.getPrice()%></p>
                                </div>
                                <div class="col-md-1" Style="text-align:left;line-height: 30px;">
                                    <span class="glyphicon glyphicon-eur" aria-hidden="true"></span>
                                </div>
                            </form>
                        </div>
                            
                        <%}                       
                    }
                %>
                
              </ul>
            </li>
            <li class="active"><a><%=money%><span class="glyphicon glyphicon-euro" aria-hidden="true"/></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
        <div class="col-md-12">
            <h3>Audios disponibles</h3>
            <%  
                for(Resource r: resources){
                    if(r.getType().equals(filetype.AUDIO)){
                   %>
                <div class="thumbnail col-md-3" Style = "margin:10px;">
                    <img src="..." alt="...">
                    <div class="caption">
                        <h3><%= r.getName()%></h3>
                        <p><%= r.getDescription()%></p>
                        <form action = ""  method="POST"  >
                            <input type='hidden' name='action' value='cart'/>
                            <% if(shopping_cart.contains(r)){ %>
                                <input type='hidden' name='remove_cart' value='<%= r.getPath() %>'/>
                                <button type="submit" class="btn btn-primary">Eliminar del carrito</button>
                            <% }else{ %>
                                <input type='hidden' name='add_cart' value='<%= r.getPath() %>'/>
                                <button type="submit" class="btn btn-primary">A�adir al carrito</button>
                            <% } %>
                        </form>
                    </div>
                </div>
                    <% } %>
                   <%
                }%>
        </div>
    </div> <!-- /container -->
  </body>
</html>
