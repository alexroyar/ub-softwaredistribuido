<%@page import="modelo.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.Resource"%>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Librer�a UB</title>
    <link href="<%= pageContext.getServletContext().getContextPath() %>/static/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/static/css/style.css"  type="text/css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}/static/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <%
        User user = (User) request.getSession(true).getAttribute("user");
        ArrayList<Resource> resources = (ArrayList<Resource>) request.getSession(true).getAttribute("resources");
        String name = user!=null? user.getName() :"Bienvenido invitado";
        String money = user!=null? String.valueOf(user.getMoney()):"~";
        String adquiridos = user!=null? String.valueOf(user.getAcquired()):"0";
        ArrayList<Resource> shopping_cart = user!=null? user.getShoppingCart():null;
    %>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Librer�a UB</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Librer�a UB</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/llibreria">Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Cat�logo<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                  <li><a href="cataleg/llibres">Libros</a></li>
                  <li><a href="cataleg/audios">Audios</a></li>
                  <li><a href="cataleg/videos">V�deos</a></li>
                  <li><a href="cataleg">Cat�logo completo</a></li>
              </ul>
            </li>
            <li><a href="http://ub-gei-sd.github.io/" target="_blank">M�s informaci�n</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                  <%=name %> <span class="caret">
                      
                  </span></a>
              <ul class="dropdown-menu" role="menu">
                   <% if(!user.isGuest()){%>
                    <li><a href="/llibreria/protegit/llista">Archivos adquiridos (<%=adquiridos%>)</a></li>
                    <li>
                        <form action = ""  method="POST" Style="margin:15px;text-align: center;">
                            <input type="hidden" name="action" value="logout"/>
                            <button type="submit" class="btn btn-primary">Cerrar sesi�n</button>
                        </form>
                    </li>
                  <%}else{%>
                    <li><a href="protegit/llista">Iniciar sesi�n</a></li>
                  <%}%>
                
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span><span class="caret"></span></a>
              <ul class="dropdown-menu col-md-12" role="menu" Style="width:500px;">
                <%if(shopping_cart.isEmpty()){%>
                        <li class="list-group-item">
                                <span class="badge">0 </span>Productos
                        </li>
                    <%}else{
                        for(Resource r : shopping_cart){
                            String name_form = "document."+r.getPath()+".submit()";
                        %>
                        <div class="col-md-12" Style="padding:10px;">
                            <form action = ""  method="POST">
                                <div class="col-md-6" Style="text-align:left;line-height: 30px;">
                                    <p><%= r.getName() %></p>
                                </div>
                                <div class="col-md-4" Style="text-align:right">
                                    <input type="hidden" name="buy_now" value="<%= r.getPath() %>"/>
                                    <button type="submit" class="btn btn-primary">Compra inmediata</button>
                                </div>
                                <div class="col-md-1" Style="text-align:right;line-height: 30px;">
                                    <p><%= r.getPrice()%></p>
                                </div>
                                <div class="col-md-1" Style="text-align:left;line-height: 30px;">
                                    <span class="glyphicon glyphicon-eur" aria-hidden="true"></span>
                                </div>
                            </form>
                        </div>
                            
                        <%}                       
                    }
                %>
                
              </ul>
            </li>
            <li class="active"><a><%=money%><span class="glyphicon glyphicon-euro" aria-hidden="true"/></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
          <div class="container">
                <div class="jumbotron col-md-12">
                    <p>Error al iniciar sesi�n, int�ntalo de nuevo.</p>
                    <form class="col-md-4 col-md-offset-4" Style="text-align: center;" method="POST" action="j_security_check">
                          <div class="input-group">
                              <input Style = "margin:5px" type="text" class="form-control centered" placeholder="Usuario" aria-describedby="basic-addon1" name="j_username"/>
                              <input Style = "margin:5px" type="password" class="form-control centered" placeholder="Password" aria-describedby="basic-addon1" name="j_password"/>
                          </div>
                          <button type="submit" class="btn btn-primary">Iniciar sesi�n</button>
                    </form>
                </div>
          </div>
  </body>
</html>
