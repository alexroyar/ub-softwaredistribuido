<%-- 
    Document   : descargas
    Created on : 27/05/2015, 10:51:10
    Author     : web
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="modelo.Resource"%>
<%@page import="modelo.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Librería UB</title>
            <link href="<%= pageContext.getServletContext().getContextPath() %>/static/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
            <link href="${pageContext.request.contextPath}/static/css/style.css"  type="text/css" rel="stylesheet">
            <script src="${pageContext.request.contextPath}/static/js/jquery.min.js"></script>
            <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
            <%
                User user = (User) request.getSession(true).getAttribute("user");
                ArrayList<Resource> resources = (ArrayList<Resource>) request.getSession(true).getAttribute("resources");
                ArrayList<Resource> acquired = user.getAllAcquired();
                String name = user!=null? user.getName() :"Bienvenido invitado";
                String money = user!=null? String.valueOf(user.getMoney()):"~";
                String adquiridos = user!=null? String.valueOf(user.getAcquired()):"0";
                ArrayList<Resource> shopping_cart = user!=null? user.getShoppingCart():null;
            %>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Librería UB</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Librería UB</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/llibreria">Home</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Catálogo<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                  <li><a href="/llibreria/cataleg/llibres">Libros</a></li>
                  <li><a href="/llibreria/cataleg/audios">Audios</a></li>
                  <li><a href="/llibreria/cataleg/videos">Vídeos</a></li>
                  <li><a href="/llibreria/cataleg">Catálogo completo</a></li>
              </ul>
            </li>
            <li><a href="http://ub-gei-sd.github.io/" target="_blank">Más información</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                  <%=name %> <span class="caret">
                      
                  </span></a>
              <ul class="dropdown-menu" role="menu">
                   <% if(!user.isGuest()){%>
                    <li><a href="/llibreria/protegit/llista">Archivos adquiridos (<%=adquiridos%>)</a></li>
                    <li>
                        <form action = ""  method="POST" Style="margin:15px;text-align: center;">
                            <input type="hidden" name="action" value="logout"/>
                            <button type="submit" class="btn btn-primary">Cerrar sesión</button>
                        </form>
                    </li>
                  <%}else{%>
                    <li><a href="/llibreria/protegit/llista">Iniciar sesión</a></li>
                  <%}%>
                
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span><span class="caret"></span></a>
              <ul class="dropdown-menu col-md-12" role="menu" Style="width:500px;">
                <%if(shopping_cart.isEmpty()){%>
                        <li class="list-group-item">
                                <span class="badge">0 </span>Productos
                        </li>
                    <%}else{
                        for(Resource r : shopping_cart){
                            String name_form = "document."+r.getPath()+".submit()";
                        %>
                        <div class="col-md-12" Style="padding:10px;">
                            <form action = ""  method="POST">
                                <div class="col-md-6" Style="text-align:left;line-height: 30px;">
                                    <p><%= r.getName() %></p>
                                </div>
                                <div class="col-md-4" Style="text-align:right">
                                    <input type="hidden" name="action" value="buy"/>
                                    <input type="hidden" name="buy_now" value="<%= r.getPath() %>"/>
                                    <button type="submit" class="btn btn-primary">Compra inmediata</button>
                                </div>
                                <div class="col-md-1" Style="text-align:right;line-height: 30px;">
                                    <p><%= r.getPrice()%></p>
                                </div>
                                <div class="col-md-1" Style="text-align:left;line-height: 30px;">
                                    <span class="glyphicon glyphicon-eur" aria-hidden="true"></span>
                                </div>
                            </form>
                        </div>
                            
                        <%}                       
                    }
                %>
                
              </ul>
            </li>
            <li class="active"><a><%=money%><span class="glyphicon glyphicon-euro" aria-hidden="true"/></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                 <h3>Descargas disponibles</h3>
                 
                <% 
                if(acquired.isEmpty()){%>
                    <h4>No has adquirido ningún recurso por el momento.</h4>
                    <p Style="padding:10px;"><span class="glyphicon glyphicon-file" aria-hidden="true" Style = "margin-right:10px;" ></span><a href="/llibreria/cataleg">Catálogo completo</a></p>
                    <p Style="padding:10px;"><span class="glyphicon glyphicon-film" aria-hidden="true" Style = "margin-right:10px;"></span><a href="/llibreria/cataleg/videos">Vídeos</a></p>
                    <p Style="padding:10px;"><span class="glyphicon glyphicon-headphones" aria-hidden="true" Style = "margin-right:10px;"></span><a href="/llibreria/cataleg/audios">Audio</a></p>
                    <p Style="padding:10px;"><span class="glyphicon glyphicon-book" aria-hidden="true" Style = "margin-right:10px;"></span><a href="/llibreria/cataleg/llibres">Books</a></p>
                    </ul>
                <%}else{
                    for(Resource r: acquired ){%>
                        <div class="thumbnail col-md-3" style="margin:8px;">
                            <img src="..." alt="...">
                            <div class="caption">
                                <h3>Nombre</h3>
                                <p>Descripcion</p>
                                <form action = ""  method="POST"  >
                                    <input type='hidden' name='action' value='download'/>
                                    <input type='hidden' name='resource' value="<%= r.getPath() %>"/>
                                    <button type="submit" class="btn btn-primary">Descargas</button>
                                </form>
                            </div>
                        </div>
                    <%}
                }%>
            </div>
        </div>
    </div>
    </body>

</html>
